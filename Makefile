BOOTLOADER_CONFIG ?=

BOARD ?= frdm_k64f

.PHONY: check boot sample_app clean_boot clean_sample_app  \
	flash_boot flash_sample_app

SIGNING_KEY ?= root-rsa-2048.pem
FIRMWARE_VERSION ?= 1.0.0

BOOT_HEADER_LEN = 0x200

FLASH_ALIGNMENT = 8

IMGTOOL = scripts/imgtool.py
PYOCD_FLASHTOOL = pyocd-flashtool

SOURCE_DIRECTORY := $(CURDIR)
BUILD_DIRECTORY := $(CURDIR)/build/$(BOARD)
BUILD_DIR_BOOT := $(BUILD_DIRECTORY)/mcuboot
BUILD_DIR_SAMPLE_APP := $(BUILD_DIRECTORY)/sample_app

all: boot sample_app

clean:  clean_sample_app
	@rm -f signed-sample_app.bin
	@rm -f mcuboot.bin

boot: check
	@rm -f mcuboot.bin
	(mkdir -p $(BUILD_DIR_BOOT) && \
		cd $(BUILD_DIR_BOOT) && \
		cmake $(BOOTLOADER_CONFIG) \
			-G"Unix Makefiles" \
			-DBOARD=$(BOARD) \
			$(SOURCE_DIRECTORY)/boot/zephyr && \
		make -j$(nproc))
	cp $(BUILD_DIR_BOOT)/zephyr/zephyr.bin mcuboot.bin

clean_boot: check
	rm -rf $(BUILD_DIR_BOOT)

# Build and sign.
sample_app: check
	(mkdir -p $(BUILD_DIR_SAMPLE_APP) && \
		cd $(BUILD_DIR_SAMPLE_APP) && \
		cmake sample_app \
			-G"Unix Makefiles" \
			-DBOARD=$(BOARD) \
			$(SOURCE_DIRECTORY)/sample_app && \
		make -j$(nproc))
	$(IMGTOOL) sign \
		--key $(SIGNING_KEY) \
		--header-size $(BOOT_HEADER_LEN) \
		--align $(FLASH_ALIGNMENT) \
		--version $(FIRMWARE_VERSION) \
		--included-header \
		$(BUILD_DIR_SAMPLE_APP)/zephyr/zephyr.bin \
		signed-sample_app.bin

clean_sample_app: check
	rm -rf $(BUILD_DIR_SAMPLE_APP)

# These flash_* targets use pyocd to flash the images.  The addresses
# are hardcoded at this time.

flash_boot:
	$(PYOCD_FLASHTOOL) -ce -a 0 mcuboot.bin

flash_sample_app:
	$(PYOCD_FLASHTOOL) -a 0x20000 signed-sample_app.bin
