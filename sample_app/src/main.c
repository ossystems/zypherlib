/*
 * Copyright (c) 2018 O.S.Systems
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define SYS_LOG_DOMAIN "Main"
#define SYS_LOG_LEVEL SYS_LOG_LEVEL_DEBUG
#include <logging/sys_log.h>
#include <zephyr.h>


void main(void){

	SYS_LOG_INF("Sample application:\n--UpdateHub init!--");
	
	updatehub_start();
	
	while(1){
		SYS_LOG_INF("TEST");
		k_sleep(1000);
	}
}
