var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var https = require('https');
var fs = require('fs');
const crypto = require('crypto');
const sign = crypto.createSign('SHA256');

app.use(bodyParser.json());

var updateMetadata = 
{
    "product-uid": "548873c4d4e8e751fdd46c38a3d5a8656cf87bf27a404f346ad58086f627a4ea",
    "supported-hardware":  ["hardware1-revA"],
    "objects": [
                { "mode":  "test",
                    "sha256sum": "539293d4aeedc0263ef6443fbd591e1c6444139f417d7f5f195e718037213881"
                }
            ]
}
var data = JSON.stringify(updateMetadata);
sign.update(data);
const privateKey = fs.readFileSync('/home/christian/updatehub-keys/private_key.pem');
const signature = sign.sign(privateKey, 'base64');

//var data = JSON.stringify(updateMetadata);  
//fs.writeFileSync('/home/christian/updatehub-keys/teste.txt', data);  
//fs.writeFileSync('/home/christian/updatehub-keys/signature.txt', signature);

var hasUpdate = true;

app.post('/report', (req, res) => {

    console.log("report");
    console.log(req.body);
    res.send("");

});

app.post('/upgrades', (req, res) => {
/*
{
  product-uid: "asdasasd",
  device-identity: { serial: "00:00:00:00:00" },
  version: 1,
  hardware: "nome da placa"
}
*/
    console.log("upgrades");
    console.log(req.body);

    if (!('product-uid' in req.body)) {
        res.status(500).send("formato invalido: faltando product-uid");
        return;
    }

    if (!('device-identity' in req.body)) {
        res.status(500).send("formato invalido: faltando device-identity");
        return;
    }

    if (typeof req.body["device-identity"] != "object") {
        res.status(500).send("formato invalido: device-identity nao eh um objecto");
        return;
    }

    if (Object.keys(req.body["device-identity"]).length == 0) {
        res.status(500).send("formato invalido: device-identity branco");
        return;
    }

    if (!('version' in req.body)) {
        res.status(500).send("formato invalido: faltando version");
        return;
    }

    if (!('hardware' in req.body)) {
        res.status(500).send("formato invalido: faltando hardware");
        return;
    }

    if (hasUpdate)
    { 
        console.log("---------------------------");
        console.log(signature);
        console.log("---------------------------");
        console.log (data);
        console.log("---------------------------");
       
        res.header('UH-Signature',signature);
        res.send(data);
    }
    else
        res.send('Update not available');
    hasUpdate = !hasUpdate;
});

app.get('/download/:product/:object', (req, res) => {
	console.log("download");
	res.sendFile('/home/christian/UPDATEHUB/updatehub-fake-server/signed-hello2.bin');
});

var httpsServer = https.createServer({                                                                                                                
   key: fs.readFileSync("ssl.key"),                                                                                                                  
   cert: fs.readFileSync("ssl.crt")                                                                                                                  
}, app);                                                                                                                                              
                                                                                                                                                     
/*httpsServer.on('secureConnection', function (tlsSocket) {                                                                                                    
 tlsSocket.setMaxSendFragment(16184);                                                                                                                  
});                                  */                                                                                                                  
                                                                                                                                                     
httpsServer.listen(8080);

