var express = require('express');
//var rawBodyParser = require('raw-body-parser');
var bodyParser = require('body-parser');
var app = express();

//app.use(rawBodyParser());
app.use(bodyParser.json());

var updateMetadata = {
    "product-uid": "548873c4d4e8e751fdd46c38a3d5a8656cf87bf27a404f346ad58086f627a4ea",
    "supported-hardware":  ["hardware1-revA"],
    "objects": [
                { "mode":  "test",
                    "sha256sum": "d0b425e00e15a0d36b9b361f02bab63563aed6cb4665083905386c55d5b679fa"
                }
            ]
}

var hasUpdate = true;

//  curl -H 'Content-Type: application/json' -X POST -d '{ "product-uid": "gustavo", "device-identity": { "serial": "ae" }, "version": 1, "hardware": "oi" }' http://localhost:8080/upgrades

app.post('/report', (req, res) => {
    /*{
           "status": "downloading",
           "package-uid": "",
           "product-uid": "",
           "device-identity": {},
           "version": "",
           "hardware": ""
    }*/
    console.log(req.body);
    res.send("ok");
});

app.post('/upgrades', (req, res) => {
/*
{
  product-uid: "asdasasd",
  device-identity: { serial: "00:00:00:00:00" },
  version: 1,
  hardware: "nome da placa"
}
*/
	//console.log(req.headers);
	//console.log(req.rawBody.toString());
    console.log(req.body);

    if (!('product-uid' in req.body)) {
        res.status(500).send("formato invalido: faltando product-uid");
        return;
    }

    if (!('device-identity' in req.body)) {
        res.status(500).send("formato invalido: faltando device-identity");
        return;
    }

    if (typeof req.body["device-identity"] != "object") {
        res.status(500).send("formato invalido: device-identity nao eh um objecto");
        return;
    }

    if (Object.keys(req.body["device-identity"]).length == 0) {
        res.status(500).send("formato invalido: device-identity branco");
        return;
    }

    if (!('version' in req.body)) {
        res.status(500).send("formato invalido: faltando version");
        return;
    }

    if (!('hardware' in req.body)) {
        res.status(500).send("formato invalido: faltando hardware");
        return;
    }

    if (hasUpdate)
	    res.json(updateMetadata);
    else
        res.send('Update not available');

	console.log("conectado");
    hasUpdate = !hasUpdate;
});

app.get('/download/:product/:object', (req, res) => {
	
	res.sendFile('/home/christian/UPDATEHUB/updatehub-fake-server/signed-hello2.bin');
});

app.listen(8080);
